window.onload = function () {
    if (window.screen.width > 767) {
        var container = document.getElementById('masonry');
        var masonry = new Masonry(container, {
            itemSelector: '.post'
        });
    }

	if (window.screen.width > 991) {
		var sidebar = document.getElementsByTagName("aside")[0];
		var footer = document.getElementsByTagName("footer")[0];
		var sidebar_offset_left = sidebar.offsetLeft;
		var sidebar_offset_top = sidebar.offsetTop;
		var sidebar_offset_height = sidebar.offsetHeight;
		var footer_offset_top = footer.offsetTop;
		window.onscroll = function () {
			var style = window.getComputedStyle(sidebar);
			var position = style.getPropertyValue('position');
			var top = style.getPropertyValue('top');
			var pageYOffset = window.pageYOffset;
			if (pageYOffset > sidebar_offset_top && (position == "static" || top == "-100px")) {
				sidebar.setAttribute("style", "position:fixed;top=0;left:" + sidebar_offset_left + "px;");
			}
			if (pageYOffset <= sidebar_offset_top && position == "fixed") {
				sidebar.setAttribute("style", "position:static;");
			}
			if (pageYOffset + sidebar_offset_height >= footer_offset_top) {
				sidebar.setAttribute("style", "position:fixed;top:-100px;left:" + sidebar_offset_left + "px;");
			}
		};
	}
}

var menu = document.getElementById("menu");
var navbar = document.getElementById("navbar");
menu.onclick = function () {
    if (navbar.style.display == "block") {
        navbar.style.display = "none";
    } else {
        navbar.style.display = "block";
    }
};